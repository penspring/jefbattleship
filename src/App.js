import React, {Component} from 'react';
import './App.css';
import battleship1 from './assets/battleship1.png';
import battleship2 from './assets/battleship2.png';
import battleship3 from './assets/battleship3.png';
import rowboat from './assets/rowboat.png';

export default class App extends Component {
  constructor(props) {
      super(props);

      this.state = {
        ships: {
          battleship1: {
            size: 4,
            chosen: false,
            anchorIsSet: false,
          },
          battleship2: {
            size: 3,
            chosen: false,
            anchorIsSet: false,
          },
          battleship3: {
            size: 2,
            chosen: false,
            anchorIsSet: false,
          },
          rowboat: {
            size: 1,
            chosen: false,
            anchorIsSet: false,
          },
        },
        cellChoices: [],
        anchors: [],
        orientation: 'vertical',
        canplay: false,
      }
  }

  setCell = (cell) => {
      const {ships, anchors, orientation} = this.state;
      let newState = Object.assign({}, this.state);
      if (newState.canPlay) {
        if (newState.cellChoices && !newState.cellChoices[cell]) {
          newState.cellChoices[cell] = this.hasAnchor(cell) ? 'hit' : 'miss';
        }
      }
      else {
        let chosenShip = ships.battleship1.chosen ? 'battleship1' : ships.battleship2.chosen ? 'battleship2' : ships.battleship3.chosen ? 'battleship3' :ships.rowboat.chosen ? 'rowboat' : '';
        if (!chosenShip) {
          alert('Please choose a ship to anchor on the grid.')
        } else {
          if (ships[chosenShip].anchorIsSet) {
            alert('This ship has been anchored already.')
          } else {
            anchors.push(cell);
            let cellCount = ships[chosenShip].size;
            if (cellCount > 1) {
                for(let i=1; i < cellCount; i++) {
                  if (orientation === 'vertical') {
                    let rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
                    let cellLetter = anchors[anchors.length-1].substring(0, 1);
                    let cellColumn = anchors[anchors.length-1].substring(1);
                    let currentRow = rows.indexOf(cellLetter)
                    cellLetter = rows[currentRow + 1 * 1];
                    anchors.push(cellLetter + cellColumn);

                  } else {
                    let cellLetter = anchors[anchors.length-1].substring(0, 1);
                    let cellColumn = anchors[anchors.length-1].substring(1);
                    cellColumn++;
                    anchors.push(cellLetter + cellColumn);
                  }
                }
            }
            newState.ships[chosenShip].anchorIsSet = true;
          }
          if (newState.ships.battleship1.anchorIsSet && newState.ships.battleship2.anchorIsSet && newState.ships.battleship3.anchorIsSet && newState.ships.rowboat.anchorIsSet) newState['canPlay'] = true;
        }
      }
      this.setState(newState);
  }

  setOrientation = (value) => {
      let newState = Object.assign({}, this.state);
      newState['orientation'] = value;
      this.setState(newState);
  }

  setChosenShip = (shipName) => {
    let ships = Object.assign({}, this.state.ships);
    ships.battleship1.chosen = false;
    ships.battleship2.chosen = false;
    ships.battleship3.chosen = false;
    ships.rowboat.chosen = false;

    if (ships[shipName].anchorIsSet) {
      alert('This ship has been anchored already.')
    } else {
      ships[shipName].chosen = true;
      this.setState({ships});
    }
  }

  hasAnchor = (cell) => {
    return this.state.anchors.indexOf(cell) > -1;
  }

  render() {
    const {ships, cellChoices, canPlay, orientation} = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <div className={'title'}>
            Jef Battleship Game
          </div>
          <div className={'instructions'}>
            {canPlay ? 'Click on a cell to determine a hit or miss' : 'Click on a battleship in order to place it on your grid'}
          </div>
          <div className={'row'}>
              <img id={'battleship1'}  src={battleship1} alt="1" className={ships.battleship1 && ships.battleship1.anchorIsSet ? 'large anchorIsSet' : ships.battleship1 && ships.battleship1.chosen ? 'large chosen' : 'large'}
                onClick={() => this.setChosenShip('battleship1')}/>
              <img id={'battleship2'}  src={battleship2} alt="2" className={ships.battleship2 && ships.battleship2.anchorIsSet ? 'medium anchorIsSet' : ships.battleship2 && ships.battleship2.chosen ? 'medium chosen' : 'medium'}
                onClick={() => this.setChosenShip('battleship2')}/>
              <img id={'battleship3'}  src={battleship3} alt="3" className={ships.battleship3 && ships.battleship3.anchorIsSet ? 'small anchorIsSet' : ships.battleship3 && ships.battleship3.chosen ? 'small chosen' : 'small'}
                onClick={() => this.setChosenShip('battleship3')}/>
              <img id={'rowboat'}  src={rowboat} alt="rowboat" className={ships.rowboat && ships.rowboat.anchorIsSet ? 'rowboat anchorIsSet' : ships.rowboat && ships.rowboat.chosen ? 'rowboat chosen' : 'rowboat'}
                onClick={() => this.setChosenShip('rowboat')}/>
          </div>
          <div className={'setHeight'}>
            {!canPlay &&
              <div className={'row'}>
                  <div className={'label'}>Orientation:</div>
                  <input type="radio" id={'orientation'} value={'vertical'} checked={orientation === 'vertical' ? true : false} onClick={() => this.setOrientation('vertical')} onChange={() => {}}/>
                  <div className={'text'}>Vertical</div>
                  <input type="radio" id={'orientation'} value={'horizontal'} checked={orientation === 'horizontal' ? true : false} onClick={() => this.setOrientation('horizontal')} onChange={() => {}}/>
                  <div className={'text'}>Horizontal</div>
              </div>
            }
          </div>
          <table border="1">
            <tbody>
              <tr>
                  <th></th>
                  <th>1</th>
                  <th>2</th>
                  <th>3</th>
                  <th>4</th>
                  <th>5</th>
                  <th>6</th>
                  <th>7</th>
                  <th>8</th>
                  <th>9</th>
                  <th>10</th>
              </tr>
              <tr>
                  <td>A</td>
                  <td id='A1' className={cellChoices.A1 === 'miss' ? 'cell blue' : cellChoices.A1 === 'hit' ? 'cell red' : this.hasAnchor('A1') ? 'cell black' : 'cell'} onClick={() => this.setCell('A1')}></td>
                  <td id='A2' className={cellChoices.A2 === 'miss' ? 'cell blue' : cellChoices.A2 === 'hit' ? 'cell red' : this.hasAnchor('A2') ? 'cell black' : 'cell'} onClick={() => this.setCell('A2')}></td>
                  <td id='A3' className={cellChoices.A3 === 'miss' ? 'cell blue' : cellChoices.A3 === 'hit' ? 'cell red' : this.hasAnchor('A3') ? 'cell black' : 'cell'} onClick={() => this.setCell('A3')}></td>
                  <td id='A4' className={cellChoices.A4 === 'miss' ? 'cell blue' : cellChoices.A4 === 'hit' ? 'cell red' : this.hasAnchor('A4') ? 'cell black' : 'cell'} onClick={() => this.setCell('A4')}></td>
                  <td id='A5' className={cellChoices.A5 === 'miss' ? 'cell blue' : cellChoices.A5 === 'hit' ? 'cell red' : this.hasAnchor('A5') ? 'cell black' : 'cell'} onClick={() => this.setCell('A5')}></td>
                  <td id='A6' className={cellChoices.A6 === 'miss' ? 'cell blue' : cellChoices.A6 === 'hit' ? 'cell red' : this.hasAnchor('A6') ? 'cell black' : 'cell'} onClick={() => this.setCell('A6')}></td>
                  <td id='A7' className={cellChoices.A7 === 'miss' ? 'cell blue' : cellChoices.A7 === 'hit' ? 'cell red' : this.hasAnchor('A7') ? 'cell black' : 'cell'} onClick={() => this.setCell('A7')}></td>
                  <td id='A8' className={cellChoices.A8 === 'miss' ? 'cell blue' : cellChoices.A8 === 'hit' ? 'cell red' : this.hasAnchor('A8') ? 'cell black' : 'cell'} onClick={() => this.setCell('A8')}></td>
                  <td id='A9' className={cellChoices.A9 === 'miss' ? 'cell blue' : cellChoices.A9 === 'hit' ? 'cell red' : this.hasAnchor('A9') ? 'cell black' : 'cell'} onClick={() => this.setCell('A9')}></td>
                  <td id='A10' className={cellChoices.A10 === 'miss' ? 'cell blue' : cellChoices.A10 === 'hit' ? 'cell red' : this.hasAnchor('A10') ? 'cell black': 'cell'} onClick={() => this.setCell('A10')}></td>
              </tr>
              <tr>
                  <td>B</td>
                  <td id='B1' className={cellChoices.B1 === 'miss' ? 'cell blue' : cellChoices.B1 === 'hit' ? 'cell red' : this.hasAnchor('B1') ? 'cell black' : 'cell'} onClick={() => this.setCell('B1')}></td>
                  <td id='B2' className={cellChoices.B2 === 'miss' ? 'cell blue' : cellChoices.B2 === 'hit' ? 'cell red' : this.hasAnchor('B2') ? 'cell black' : 'cell'} onClick={() => this.setCell('B2')}></td>
                  <td id='B3' className={cellChoices.B3 === 'miss' ? 'cell blue' : cellChoices.B3 === 'hit' ? 'cell red' : this.hasAnchor('B3') ? 'cell black' : 'cell'} onClick={() => this.setCell('B3')}></td>
                  <td id='B4' className={cellChoices.B4 === 'miss' ? 'cell blue' : cellChoices.B4 === 'hit' ? 'cell red' : this.hasAnchor('B4') ? 'cell black' : 'cell'} onClick={() => this.setCell('B4')}></td>
                  <td id='B5' className={cellChoices.B5 === 'miss' ? 'cell blue' : cellChoices.B5 === 'hit' ? 'cell red' : this.hasAnchor('B5') ? 'cell black' : 'cell'} onClick={() => this.setCell('B5')}></td>
                  <td id='B6' className={cellChoices.B6 === 'miss' ? 'cell blue' : cellChoices.B6 === 'hit' ? 'cell red' : this.hasAnchor('B6') ? 'cell black' : 'cell'} onClick={() => this.setCell('B6')}></td>
                  <td id='B7' className={cellChoices.B7 === 'miss' ? 'cell blue' : cellChoices.B7 === 'hit' ? 'cell red' : this.hasAnchor('B7') ? 'cell black' : 'cell'} onClick={() => this.setCell('B7')}></td>
                  <td id='B8' className={cellChoices.B8 === 'miss' ? 'cell blue' : cellChoices.B8 === 'hit' ? 'cell red' : this.hasAnchor('B8') ? 'cell black' : 'cell'} onClick={() => this.setCell('B8')}></td>
                  <td id='B9' className={cellChoices.B9 === 'miss' ? 'cell blue' : cellChoices.B9 === 'hit' ? 'cell red' : this.hasAnchor('B9') ? 'cell black' : 'cell'} onClick={() => this.setCell('B9')}></td>
                  <td id='B10' className={cellChoices.B10 === 'miss' ? 'cell blue' : cellChoices.B10 === 'hit' ? 'cell red' : this.hasAnchor('B10') ? 'cell black': 'cell'} onClick={() => this.setCell('B10')}></td>
              </tr>
              <tr>
                  <td>C</td>
                  <td id='C1' className={cellChoices.C1 === 'miss' ? 'cell blue' : cellChoices.C1 === 'hit' ? 'cell red' : this.hasAnchor('C1') ? 'cell black' : 'cell'} onClick={() => this.setCell('C1')}></td>
                  <td id='C2' className={cellChoices.C2 === 'miss' ? 'cell blue' : cellChoices.C2 === 'hit' ? 'cell red' : this.hasAnchor('C2') ? 'cell black' : 'cell'} onClick={() => this.setCell('C2')}></td>
                  <td id='C3' className={cellChoices.C3 === 'miss' ? 'cell blue' : cellChoices.C3 === 'hit' ? 'cell red' : this.hasAnchor('C3') ? 'cell black' : 'cell'} onClick={() => this.setCell('C3')}></td>
                  <td id='C4' className={cellChoices.C4 === 'miss' ? 'cell blue' : cellChoices.C4 === 'hit' ? 'cell red' : this.hasAnchor('C4') ? 'cell black' : 'cell'} onClick={() => this.setCell('C4')}></td>
                  <td id='C5' className={cellChoices.C5 === 'miss' ? 'cell blue' : cellChoices.C5 === 'hit' ? 'cell red' : this.hasAnchor('C5') ? 'cell black' : 'cell'} onClick={() => this.setCell('C5')}></td>
                  <td id='C6' className={cellChoices.C6 === 'miss' ? 'cell blue' : cellChoices.C6 === 'hit' ? 'cell red' : this.hasAnchor('C6') ? 'cell black' : 'cell'} onClick={() => this.setCell('C6')}></td>
                  <td id='C7' className={cellChoices.C7 === 'miss' ? 'cell blue' : cellChoices.C7 === 'hit' ? 'cell red' : this.hasAnchor('C7') ? 'cell black' : 'cell'} onClick={() => this.setCell('C7')}></td>
                  <td id='C8' className={cellChoices.C8 === 'miss' ? 'cell blue' : cellChoices.C8 === 'hit' ? 'cell red' : this.hasAnchor('C8') ? 'cell black' : 'cell'} onClick={() => this.setCell('C8')}></td>
                  <td id='C9' className={cellChoices.C9 === 'miss' ? 'cell blue' : cellChoices.C9 === 'hit' ? 'cell red' : this.hasAnchor('C9') ? 'cell black' : 'cell'} onClick={() => this.setCell('C9')}></td>
                  <td id='C10' className={cellChoices.C10 === 'miss' ? 'cell blue' : cellChoices.C10 === 'hit' ? 'cell red' : this.hasAnchor('C10') ? 'cell black': 'cell'} onClick={() => this.setCell('C10')}></td>
              </tr>
              <tr>
                  <td>D</td>
                  <td id='D1' className={cellChoices.D1 === 'miss' ? 'cell blue' : cellChoices.D1 === 'hit' ? 'cell red' : this.hasAnchor('D1') ? 'cell black' : 'cell'} onClick={() => this.setCell('D1')}></td>
                  <td id='D2' className={cellChoices.D2 === 'miss' ? 'cell blue' : cellChoices.D2 === 'hit' ? 'cell red' : this.hasAnchor('D2') ? 'cell black' : 'cell'} onClick={() => this.setCell('D2')}></td>
                  <td id='D3' className={cellChoices.D3 === 'miss' ? 'cell blue' : cellChoices.D3 === 'hit' ? 'cell red' : this.hasAnchor('D3') ? 'cell black' : 'cell'} onClick={() => this.setCell('D3')}></td>
                  <td id='D4' className={cellChoices.D4 === 'miss' ? 'cell blue' : cellChoices.D4 === 'hit' ? 'cell red' : this.hasAnchor('D4') ? 'cell black' : 'cell'} onClick={() => this.setCell('D4')}></td>
                  <td id='D5' className={cellChoices.D5 === 'miss' ? 'cell blue' : cellChoices.D5 === 'hit' ? 'cell red' : this.hasAnchor('D5') ? 'cell black' : 'cell'} onClick={() => this.setCell('D5')}></td>
                  <td id='D6' className={cellChoices.D6 === 'miss' ? 'cell blue' : cellChoices.D6 === 'hit' ? 'cell red' : this.hasAnchor('D6') ? 'cell black' : 'cell'} onClick={() => this.setCell('D6')}></td>
                  <td id='D7' className={cellChoices.D7 === 'miss' ? 'cell blue' : cellChoices.D7 === 'hit' ? 'cell red' : this.hasAnchor('D7') ? 'cell black' : 'cell'} onClick={() => this.setCell('D7')}></td>
                  <td id='D8' className={cellChoices.D8 === 'miss' ? 'cell blue' : cellChoices.D8 === 'hit' ? 'cell red' : this.hasAnchor('D8') ? 'cell black' : 'cell'} onClick={() => this.setCell('D8')}></td>
                  <td id='D9' className={cellChoices.D9 === 'miss' ? 'cell blue' : cellChoices.D9 === 'hit' ? 'cell red' : this.hasAnchor('D9') ? 'cell black' : 'cell'} onClick={() => this.setCell('D9')}></td>
                  <td id='D10' className={cellChoices.D10 === 'miss' ? 'cell blue' : cellChoices.D10 === 'hit' ? 'cell red' : this.hasAnchor('D10') ? 'cell black': 'cell'} onClick={() => this.setCell('D10')}></td>
              </tr>
              <tr>
                  <td>E</td>
                  <td id='E1' className={cellChoices.E1 === 'miss' ? 'cell blue' : cellChoices.E1 === 'hit' ? 'cell red' : this.hasAnchor('E1') ? 'cell black' : 'cell'} onClick={() => this.setCell('E1')}></td>
                  <td id='E2' className={cellChoices.E2 === 'miss' ? 'cell blue' : cellChoices.E2 === 'hit' ? 'cell red' : this.hasAnchor('E2') ? 'cell black' : 'cell'} onClick={() => this.setCell('E2')}></td>
                  <td id='E3' className={cellChoices.E3 === 'miss' ? 'cell blue' : cellChoices.E3 === 'hit' ? 'cell red' : this.hasAnchor('E3') ? 'cell black' : 'cell'} onClick={() => this.setCell('E3')}></td>
                  <td id='E4' className={cellChoices.E4 === 'miss' ? 'cell blue' : cellChoices.E4 === 'hit' ? 'cell red' : this.hasAnchor('E4') ? 'cell black' : 'cell'} onClick={() => this.setCell('E4')}></td>
                  <td id='E5' className={cellChoices.E5 === 'miss' ? 'cell blue' : cellChoices.E5 === 'hit' ? 'cell red' : this.hasAnchor('E5') ? 'cell black' : 'cell'} onClick={() => this.setCell('E5')}></td>
                  <td id='E6' className={cellChoices.E6 === 'miss' ? 'cell blue' : cellChoices.E6 === 'hit' ? 'cell red' : this.hasAnchor('E6') ? 'cell black' : 'cell'} onClick={() => this.setCell('E6')}></td>
                  <td id='E7' className={cellChoices.E7 === 'miss' ? 'cell blue' : cellChoices.E7 === 'hit' ? 'cell red' : this.hasAnchor('E7') ? 'cell black' : 'cell'} onClick={() => this.setCell('E7')}></td>
                  <td id='E8' className={cellChoices.E8 === 'miss' ? 'cell blue' : cellChoices.E8 === 'hit' ? 'cell red' : this.hasAnchor('E8') ? 'cell black' : 'cell'} onClick={() => this.setCell('E8')}></td>
                  <td id='E9' className={cellChoices.E9 === 'miss' ? 'cell blue' : cellChoices.E9 === 'hit' ? 'cell red' : this.hasAnchor('E9') ? 'cell black' : 'cell'} onClick={() => this.setCell('E9')}></td>
                  <td id='E10' className={cellChoices.E10 === 'miss' ? 'cell blue' : cellChoices.E10 === 'hit' ? 'cell red' : this.hasAnchor('E10') ? 'cell black': 'cell'} onClick={() => this.setCell('E10')}></td>
              </tr>
              <tr>
                  <td>F</td>
                  <td id='F1' className={cellChoices.F1 === 'miss' ? 'cell blue' : cellChoices.F1 === 'hit' ? 'cell red' : this.hasAnchor('F1') ? 'cell black' : 'cell'} onClick={() => this.setCell('F1')}></td>
                  <td id='F2' className={cellChoices.F2 === 'miss' ? 'cell blue' : cellChoices.F2 === 'hit' ? 'cell red' : this.hasAnchor('F2') ? 'cell black' : 'cell'} onClick={() => this.setCell('F2')}></td>
                  <td id='F3' className={cellChoices.F3 === 'miss' ? 'cell blue' : cellChoices.F3 === 'hit' ? 'cell red' : this.hasAnchor('F3') ? 'cell black' : 'cell'} onClick={() => this.setCell('F3')}></td>
                  <td id='F4' className={cellChoices.F4 === 'miss' ? 'cell blue' : cellChoices.F4 === 'hit' ? 'cell red' : this.hasAnchor('F4') ? 'cell black' : 'cell'} onClick={() => this.setCell('F4')}></td>
                  <td id='F5' className={cellChoices.F5 === 'miss' ? 'cell blue' : cellChoices.F5 === 'hit' ? 'cell red' : this.hasAnchor('F5') ? 'cell black' : 'cell'} onClick={() => this.setCell('F5')}></td>
                  <td id='F6' className={cellChoices.F6 === 'miss' ? 'cell blue' : cellChoices.F6 === 'hit' ? 'cell red' : this.hasAnchor('F6') ? 'cell black' : 'cell'} onClick={() => this.setCell('F6')}></td>
                  <td id='F7' className={cellChoices.F7 === 'miss' ? 'cell blue' : cellChoices.F7 === 'hit' ? 'cell red' : this.hasAnchor('F7') ? 'cell black' : 'cell'} onClick={() => this.setCell('F7')}></td>
                  <td id='F8' className={cellChoices.F8 === 'miss' ? 'cell blue' : cellChoices.F8 === 'hit' ? 'cell red' : this.hasAnchor('F8') ? 'cell black' : 'cell'} onClick={() => this.setCell('F8')}></td>
                  <td id='F9' className={cellChoices.F9 === 'miss' ? 'cell blue' : cellChoices.F9 === 'hit' ? 'cell red' : this.hasAnchor('F9') ? 'cell black' : 'cell'} onClick={() => this.setCell('F9')}></td>
                  <td id='F10' className={cellChoices.F10 === 'miss' ? 'cell blue' : cellChoices.F10 === 'hit' ? 'cell red' : this.hasAnchor('F10') ? 'cell black': 'cell'} onClick={() => this.setCell('F10')}></td>
              </tr>
              <tr>
                  <td>G</td>
                  <td id='G1' className={cellChoices.G1 === 'miss' ? 'cell blue' : cellChoices.G1 === 'hit' ? 'cell red' : this.hasAnchor('G1') ? 'cell black' : 'cell'} onClick={() => this.setCell('G1')}></td>
                  <td id='G2' className={cellChoices.G2 === 'miss' ? 'cell blue' : cellChoices.G2 === 'hit' ? 'cell red' : this.hasAnchor('G2') ? 'cell black' : 'cell'} onClick={() => this.setCell('G2')}></td>
                  <td id='G3' className={cellChoices.G3 === 'miss' ? 'cell blue' : cellChoices.G3 === 'hit' ? 'cell red' : this.hasAnchor('G3') ? 'cell black' : 'cell'} onClick={() => this.setCell('G3')}></td>
                  <td id='G4' className={cellChoices.G4 === 'miss' ? 'cell blue' : cellChoices.G4 === 'hit' ? 'cell red' : this.hasAnchor('G4') ? 'cell black' : 'cell'} onClick={() => this.setCell('G4')}></td>
                  <td id='G5' className={cellChoices.G5 === 'miss' ? 'cell blue' : cellChoices.G5 === 'hit' ? 'cell red' : this.hasAnchor('G5') ? 'cell black' : 'cell'} onClick={() => this.setCell('G5')}></td>
                  <td id='G6' className={cellChoices.G6 === 'miss' ? 'cell blue' : cellChoices.G6 === 'hit' ? 'cell red' : this.hasAnchor('G6') ? 'cell black' : 'cell'} onClick={() => this.setCell('G6')}></td>
                  <td id='G7' className={cellChoices.G7 === 'miss' ? 'cell blue' : cellChoices.G7 === 'hit' ? 'cell red' : this.hasAnchor('G7') ? 'cell black' : 'cell'} onClick={() => this.setCell('G7')}></td>
                  <td id='G8' className={cellChoices.G8 === 'miss' ? 'cell blue' : cellChoices.G8 === 'hit' ? 'cell red' : this.hasAnchor('G8') ? 'cell black' : 'cell'} onClick={() => this.setCell('G8')}></td>
                  <td id='G9' className={cellChoices.G9 === 'miss' ? 'cell blue' : cellChoices.G9 === 'hit' ? 'cell red' : this.hasAnchor('G9') ? 'cell black' : 'cell'} onClick={() => this.setCell('G9')}></td>
                  <td id='G10' className={cellChoices.G10 === 'miss' ? 'cell blue' : cellChoices.G10 === 'hit' ? 'cell red' : this.hasAnchor('G10') ? 'cell black': 'cell'} onClick={() => this.setCell('G10')}></td>
              </tr>
              <tr>
                  <td>H</td>
                  <td id='H1' className={cellChoices.H1 === 'miss' ? 'cell blue' : cellChoices.H1 === 'hit' ? 'cell red' : this.hasAnchor('H1') ? 'cell black' : 'cell'} onClick={() => this.setCell('H1')}></td>
                  <td id='H2' className={cellChoices.H2 === 'miss' ? 'cell blue' : cellChoices.H2 === 'hit' ? 'cell red' : this.hasAnchor('H2') ? 'cell black' : 'cell'} onClick={() => this.setCell('H2')}></td>
                  <td id='H3' className={cellChoices.H3 === 'miss' ? 'cell blue' : cellChoices.H3 === 'hit' ? 'cell red' : this.hasAnchor('H3') ? 'cell black' : 'cell'} onClick={() => this.setCell('H3')}></td>
                  <td id='H4' className={cellChoices.H4 === 'miss' ? 'cell blue' : cellChoices.H4 === 'hit' ? 'cell red' : this.hasAnchor('H4') ? 'cell black' : 'cell'} onClick={() => this.setCell('H4')}></td>
                  <td id='H5' className={cellChoices.H5 === 'miss' ? 'cell blue' : cellChoices.H5 === 'hit' ? 'cell red' : this.hasAnchor('H5') ? 'cell black' : 'cell'} onClick={() => this.setCell('H5')}></td>
                  <td id='H6' className={cellChoices.H6 === 'miss' ? 'cell blue' : cellChoices.H6 === 'hit' ? 'cell red' : this.hasAnchor('H6') ? 'cell black' : 'cell'} onClick={() => this.setCell('H6')}></td>
                  <td id='H7' className={cellChoices.H7 === 'miss' ? 'cell blue' : cellChoices.H7 === 'hit' ? 'cell red' : this.hasAnchor('H7') ? 'cell black' : 'cell'} onClick={() => this.setCell('H7')}></td>
                  <td id='H8' className={cellChoices.H8 === 'miss' ? 'cell blue' : cellChoices.H8 === 'hit' ? 'cell red' : this.hasAnchor('H8') ? 'cell black' : 'cell'} onClick={() => this.setCell('H8')}></td>
                  <td id='H9' className={cellChoices.H9 === 'miss' ? 'cell blue' : cellChoices.H9 === 'hit' ? 'cell red' : this.hasAnchor('H9') ? 'cell black' : 'cell'} onClick={() => this.setCell('H9')}></td>
                  <td id='H10' className={cellChoices.H10 === 'miss' ? 'cell blue' : cellChoices.H10 === 'hit' ? 'cell red' : this.hasAnchor('H10') ? 'cell black': 'cell'} onClick={() => this.setCell('H10')}></td>
              </tr>
              <tr>
                  <td>I</td>
                  <td id='I1' className={cellChoices.I1 === 'miss' ? 'cell blue' : cellChoices.I1 === 'hit' ? 'cell red' : this.hasAnchor('I1') ? 'cell black' : 'cell'} onClick={() => this.setCell('I1')}></td>
                  <td id='I2' className={cellChoices.I2 === 'miss' ? 'cell blue' : cellChoices.I2 === 'hit' ? 'cell red' : this.hasAnchor('I2') ? 'cell black' : 'cell'} onClick={() => this.setCell('I2')}></td>
                  <td id='I3' className={cellChoices.I3 === 'miss' ? 'cell blue' : cellChoices.I3 === 'hit' ? 'cell red' : this.hasAnchor('I3') ? 'cell black' : 'cell'} onClick={() => this.setCell('I3')}></td>
                  <td id='I4' className={cellChoices.I4 === 'miss' ? 'cell blue' : cellChoices.I4 === 'hit' ? 'cell red' : this.hasAnchor('I4') ? 'cell black' : 'cell'} onClick={() => this.setCell('I4')}></td>
                  <td id='I5' className={cellChoices.I5 === 'miss' ? 'cell blue' : cellChoices.I5 === 'hit' ? 'cell red' : this.hasAnchor('I5') ? 'cell black' : 'cell'} onClick={() => this.setCell('I5')}></td>
                  <td id='I6' className={cellChoices.I6 === 'miss' ? 'cell blue' : cellChoices.I6 === 'hit' ? 'cell red' : this.hasAnchor('I6') ? 'cell black' : 'cell'} onClick={() => this.setCell('I6')}></td>
                  <td id='I7' className={cellChoices.I7 === 'miss' ? 'cell blue' : cellChoices.I7 === 'hit' ? 'cell red' : this.hasAnchor('I7') ? 'cell black' : 'cell'} onClick={() => this.setCell('I7')}></td>
                  <td id='I8' className={cellChoices.I8 === 'miss' ? 'cell blue' : cellChoices.I8 === 'hit' ? 'cell red' : this.hasAnchor('I8') ? 'cell black' : 'cell'} onClick={() => this.setCell('I8')}></td>
                  <td id='I9' className={cellChoices.I9 === 'miss' ? 'cell blue' : cellChoices.I9 === 'hit' ? 'cell red' : this.hasAnchor('I9') ? 'cell black' : 'cell'} onClick={() => this.setCell('I9')}></td>
                  <td id='I10' className={cellChoices.I10 === 'miss' ? 'cell blue' : cellChoices.I10 === 'hit' ? 'cell red' : this.hasAnchor('I10') ? 'cell black': 'cell'} onClick={() => this.setCell('I10')}></td>
              </tr>
              <tr>
                  <td>J</td>
                  <td id='J1' className={cellChoices.J1 === 'miss' ? 'cell blue' : cellChoices.J1 === 'hit' ? 'cell red' : this.hasAnchor('J1') ? 'cell black' : 'cell'} onClick={() => this.setCell('J1')}></td>
                  <td id='J2' className={cellChoices.J2 === 'miss' ? 'cell blue' : cellChoices.J2 === 'hit' ? 'cell red' : this.hasAnchor('J2') ? 'cell black' : 'cell'} onClick={() => this.setCell('J2')}></td>
                  <td id='J3' className={cellChoices.J3 === 'miss' ? 'cell blue' : cellChoices.J3 === 'hit' ? 'cell red' : this.hasAnchor('J3') ? 'cell black' : 'cell'} onClick={() => this.setCell('J3')}></td>
                  <td id='J4' className={cellChoices.J4 === 'miss' ? 'cell blue' : cellChoices.J4 === 'hit' ? 'cell red' : this.hasAnchor('J4') ? 'cell black' : 'cell'} onClick={() => this.setCell('J4')}></td>
                  <td id='J5' className={cellChoices.J5 === 'miss' ? 'cell blue' : cellChoices.J5 === 'hit' ? 'cell red' : this.hasAnchor('J5') ? 'cell black' : 'cell'} onClick={() => this.setCell('J5')}></td>
                  <td id='J6' className={cellChoices.J6 === 'miss' ? 'cell blue' : cellChoices.J6 === 'hit' ? 'cell red' : this.hasAnchor('J6') ? 'cell black' : 'cell'} onClick={() => this.setCell('J6')}></td>
                  <td id='J7' className={cellChoices.J7 === 'miss' ? 'cell blue' : cellChoices.J7 === 'hit' ? 'cell red' : this.hasAnchor('J7') ? 'cell black' : 'cell'} onClick={() => this.setCell('J7')}></td>
                  <td id='J8' className={cellChoices.J8 === 'miss' ? 'cell blue' : cellChoices.J8 === 'hit' ? 'cell red' : this.hasAnchor('J8') ? 'cell black' : 'cell'} onClick={() => this.setCell('J8')}></td>
                  <td id='J9' className={cellChoices.J9 === 'miss' ? 'cell blue' : cellChoices.J9 === 'hit' ? 'cell red' : this.hasAnchor('J9') ? 'cell black' : 'cell'} onClick={() => this.setCell('J9')}></td>
                  <td id='J10' className={cellChoices.J10 === 'miss' ? 'cell blue' : cellChoices.J10 === 'hit' ? 'cell red' : this.hasAnchor('J10') ? 'cell black': 'cell'} onClick={() => this.setCell('J10')}></td>
              </tr>
            </tbody>
          </table>
        </header>
      </div>
    );
  }
}
